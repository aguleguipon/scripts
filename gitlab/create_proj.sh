#Tomo variable cuando llamo al programa
archivo=$1
token=$2
namespace=$3
visibility=$4

usage() {
    echo ""
    echo "Uso: $0 <ARCHIVO-CON-LISTADO> <TOKEN-ID> <NAMESPACE> <PRIVATE/INTERNAL/PUBLIC>"
    exit 1
}

[ -z "$archivo" ] && usage
[ -z "$token" ] && usage
[ -z "$namespace" ] && usage
[ -z "$visibility" ] && usage


#Chequeamos que la privacidad sea un valor correcto
if [ "$visibility" == "private" ]; then
    echo "Visibilidad Privada";
else
    if [ "$visibility" == "internal" ]; then
        echo "Visibilidad Intarna";
    else 
        if [ "$visibility" == "public" ]; then
            echo "Visibilidad Publica";
        else
        echo "NO ES CORRECTA LA PRIVACIDAD"
        exit 1
        fi
    fi
fi

#Itera por cada línea del archivo que definimos que tendrán la lista de repositorios
while IFS= read -r nombre || [ -n "$nombre" ]
do
    #nombre=${nombre%?}
    #Construcción de nombre
    data='{"name":"'"$nombre"'","path":"'"$nombre"'","namespace_id":"'"$namespace"'","visibility":"private"}'
    curl -H "Content-Type: application/json" -H "PRIVATE-TOKEN: $token" -X POST -d $data https://gitlab.com/api/v4/projects
	#Preparo URL para clonar
    repo='https://gitlab.com/appdespliegue/'$nombre'.git'
	#Clono el repositorio
    git clone $repo
	cd $nombre
    #Creo un archivo inicial
	touch init.txt
    #Lo subo a master
	git add .
	git commit -m "Creación inicial"
	git push origin master
    #Lo elimino
	rm init.txt
	git add .
	git commit -m "Creación inicial"
	git push origin master
	cd ..
    #Borro la carpeta para no tener carpetas residuales
	rm -fr $nombre
done < "$archivo"