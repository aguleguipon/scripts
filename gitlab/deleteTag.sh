#Tomo variable cuando llamo al programa
token=$1
proyecto=$2
namespace=$3
tag=$4


usage() {
    echo ""
    echo "Uso: sh $0 <TOKEN> <PROYECTO> <GRUPO/SUBGRUPO o NAMESPACE_ID> <TAG>"
    exit 1
}

[ -z "$token" ] && usage
[ -z "$proyecto" ] && usage
[ -z "$namespace" ] && usage
http="https://gitlab.com/api/v4/projects/"$namespace"%2F"$proyecto"/repository/tags/"$tag
curl --request DELETE -H "Content-Type: application/json" -H "PRIVATE-TOKEN: $token" $http
echo $http